package com.example.demo;

import com.example.demo.services.MenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	MenuService menuService;

	@Test
	void testGetItemPriceSum() {
		assertTrue(new BigDecimal(10).equals(menuService.getItemPriceSum(menuService.getMenus(null).get(0))));
	}

	@Test
	void testGetActiveSubMenuCount() {
		assertEquals(0L, menuService.getActiveSubMenuCount(menuService.getMenus(null).get(0)));
	}

}
