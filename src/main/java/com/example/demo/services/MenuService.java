package com.example.demo.services;

import com.example.demo.constants.MenuConstants;
import com.example.demo.domains.*;
import com.example.demo.utils.MenuUtils;
import com.example.demo.utils.loggers.LogExecProfiler;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MenuService {

    /** 7. The application should log all the execution time for every method or a group of them. [via @LogExecProfiler]
     * 2. Rest Endpoint. The application should list the menus to be consumed by the front end.
     * @param particularMenu
     * @return
     */
    @LogExecProfiler
    public List<Menu> getMenus(ArrayList<String> particularMenu) {
        List<Menu> menus = new ArrayList<>();

        ArrayList<Item> breakFastItems = new ArrayList<>();
        Item breakFastItem = new Item("french toast",
                "you will fall in love with life",
                new BigDecimal(10.0),
                "INR",
                "http://lorempixel.com/400/200/food/",
                new ServiceAvailability(
                        Arrays.asList(Days.values()),
                        true,
                        new TimePeriod(LocalDate.now(), LocalDate.now().plusDays(7), LocalTime.of(8, 0, 0), LocalTime.of(12, 0, 0))),
                1
        );
        breakFastItems.add(breakFastItem);
        menus.add(new Menu("Breakfast Menu", true, breakFastItems, null));

        ArrayList<Item> lunchItems = new ArrayList<>();
        Item lunchItem = new Item("Pizza",
                "now all you need is me",
                new BigDecimal(100.0),
                "INR",
                "http://lorempixel.com/400/200/food/",
                new ServiceAvailability(
                        Arrays.asList(Days.values()),
                        true,
                        new TimePeriod(LocalDate.now(), LocalDate.now().plusDays(7), LocalTime.of(12, 1, 0), LocalTime.of(16, 0, 0))),
                1
        );
        lunchItems.add(lunchItem);
        menus.add(new Menu("Lunch Menu", true, lunchItems, null));


        ArrayList<Item> dinnerItems = new ArrayList<>();
        Item dinnerItem = new Item("Sushi",
                "graceful shutdown",
                new BigDecimal(100.0),
                "INR",
                "http://lorempixel.com/400/200/food/",
                new ServiceAvailability(
                        Arrays.asList(Days.values()),
                        true,
                        new TimePeriod(LocalDate.now(), LocalDate.now().plusDays(7), LocalTime.of(16, 1, 0), LocalTime.of(20, 0, 0))),
                1
        );
        dinnerItems.add(dinnerItem);
        menus.add(new Menu("Dinner Menu", true, dinnerItems, null));

        // filter for required menu
        if(!MenuUtils.isNullOrEmpty(particularMenu)) {
            menus = menus.stream()
                    .filter(menu -> particularMenu.stream().anyMatch(pmenu->pmenu.trim().equalsIgnoreCase(menu.getDescription().trim())))
                    .collect(Collectors.toList());
        }

        return menus;
    }

    /**
     * 3. Rest Endpoint: Given a menu, the application should list the items grouped by price.
     * Take into consideration that in the future, it can be grouped by ranking.
     * @param groupBy
     * @param menu
     * @return
     */
    public Map getGroupedItems(String groupBy, Menu menu) {
        Map items;
        List<Item> allItems = new ArrayList<>();
        if(!MenuUtils.isNullOrEmpty(menu.getItems())) {
            allItems.addAll(menu.getItems());
        }
        if(!MenuUtils.isNullOrEmpty(menu.getSubMenus())) {
            allItems.addAll(menu.getSubMenus().stream().flatMap(subMenu-> subMenu.getItems().stream()).collect(Collectors.toList()));
        }
        items = MenuUtils.groupBy(allItems, Item::getPrice);
        if(groupBy.equalsIgnoreCase(MenuConstants.GROUP_BY_RANK)) {
            items = MenuUtils.groupBy(allItems, Item::getRank);
        }
        return items;
    }

    /**
     * 4. Method (*): given a menu, the application should return the sum of prices of all its items (including submenus).
     * @param menu
     * @return
     */
    public BigDecimal getItemPriceSum(Menu menu) {
        List<Item> allItems = new ArrayList<>();
        if(!MenuUtils.isNullOrEmpty(menu.getItems())) {
            allItems.addAll(menu.getItems());
        }
        if(!MenuUtils.isNullOrEmpty(menu.getSubMenus())) {
            allItems.addAll(menu.getSubMenus().stream().flatMap(subMenu-> subMenu.getItems().stream()).collect(Collectors.toList()));
        }
        return allItems.stream()
                .map(Item::getPrice)
                .reduce(BigDecimal::add)
                .get();
    }

    /**
     * 5. Method (*): given a menu, the application should return the quantity of active submenus
     * @param menu
     * @return
     */
    public long getActiveSubMenuCount(Menu menu) {
        if(MenuUtils.isNullOrEmpty(menu.getSubMenus())){
            return 0;
        }
        return menu.getSubMenus().stream()
                .filter(Menu::isActive)
                .count();
    }
}
