package com.example.demo.utils.loggers;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class ExecProfilerLoggingAspect {

    private static Logger logger = Logger.getLogger(ExecProfilerLoggingAspect.class.getName());

    @Pointcut("@annotation(com.example.demo.utils.loggers.LogExecProfiler)")
    public void loggable(){}

    @Around("loggable()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        Object output;
        try {
            output = pjp.proceed();
        } finally {
            long endTime = System.currentTimeMillis();
            long elapsedTime = endTime - start;
            logger.info("Method execution completed: " + pjp.getSignature().getName() + ", started at(millis): " + start + ", completed at(millis): " + endTime + ", execution time(millis): " + elapsedTime);
        }
        return output;
    }
}


