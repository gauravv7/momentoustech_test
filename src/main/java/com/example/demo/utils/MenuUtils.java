package com.example.demo.utils;

import com.example.demo.domains.Menu;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MenuUtils {

    public static <T, K, V> Map<K, List<T>> groupBy(
            Iterable<T> list,
            Function<T, K> keyMapper) {
        Stream<T> stream = StreamSupport.stream(list.spliterator(), false);
        return stream.collect(Collectors.groupingBy(keyMapper));
    }

    public static boolean isNullOrEmpty(List subMenus) {
        if(null==subMenus)
            return true;
        return subMenus.isEmpty();
    }
}
