package com.example.demo.controllers;

import com.example.demo.domains.Item;
import com.example.demo.domains.Menu;
import com.example.demo.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/menu/v1")
public class MenuController {


    @Autowired
    MenuService menuService;

    @RequestMapping(value = "ping")
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("pong");
    }


    /** fetches all available menus or a list of menu(s) asked
     * curl 'http://localhost:8080/api/menu/v1/menus'
     * curl 'http://localhost:8080/api/menu/v1/menus?menus=Dinner%20Menu&menus=Lunch%20Menu'
     * @param particularMenu OPTIONAL- menu filter
     * @return list of menus
     */
    @RequestMapping(value = "/menus", method = RequestMethod.GET)
    public ResponseEntity<List<Menu>> listMenus(@RequestParam(value = "menus", required = false) ArrayList<String> particularMenu) {
        List<Menu> menus = menuService.getMenus(particularMenu);
        return ResponseEntity.ok(menus);

    }


    /** given a menu, groups the items basis given grouping factor
     * curl 'http://localhost:8080/api/menu/v1/menus'
     * curl 'http://localhost:8080/api/menu/v1/menus?menus=Dinner%20Menu&menus=Lunch%20Menu'
     * @param groupBy - menu item grouping factor [price/rank]
     * @return list of menus
     */
    @RequestMapping(value = "/menu", method = RequestMethod.PUT)
    public ResponseEntity<Map> showClubbedItem(@RequestParam(value = "groupBy", required = false) String groupBy, @RequestBody Menu menu) {
        Map items = menuService.getGroupedItems(groupBy, menu);
        return ResponseEntity.ok(items);
    }

}
