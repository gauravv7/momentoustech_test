package com.example.demo.domains;

import java.math.BigDecimal;

public class Item {

    String name;
    String description;
    BigDecimal price;
    String currency;
    String photo;
    ServiceAvailability serviceAvailability;
    int rank;

    public Item(String name, String description, BigDecimal price, String currency, String photo, ServiceAvailability serviceAvailability, int rank) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.currency = currency;
        this.photo = photo;
        this.serviceAvailability = serviceAvailability;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ServiceAvailability getServiceAvailability() {
        return serviceAvailability;
    }

    public void setServiceAvailability(ServiceAvailability serviceAvailability) {
        this.serviceAvailability = serviceAvailability;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", currency='" + currency + '\'' +
                ", photo='" + photo + '\'' +
                ", serviceAvailability=" + serviceAvailability +
                ", rank=" + rank +
                '}';
    }
}
