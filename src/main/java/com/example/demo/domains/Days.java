package com.example.demo.domains;

import java.util.ArrayList;
import java.util.List;

public enum Days {
    MONDAY("MONDAY"),
    TUESDAY("TUESDAY"),
    WEDNESDAY("WEDNESDAY"),
    THURSDAY("THURSDAY"),
    FRIDAY("FRIDAY"),
    SATURDAY("SATURDAY"),
    SUNDAY("SUNDAY");

    String value;

    Days(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static List<String> getAllDaysList() {
        ArrayList<String> dayAsStings = new ArrayList<>();
        for (Days value : Days.values()) {
            dayAsStings.add(value.getValue());
        }
        return dayAsStings;
    }
}
