package com.example.demo.domains;

import java.util.List;

public class Menu {

    private String description;
    private boolean active;
    private List<Item> items;
    private List<Menu> subMenus;

    public Menu(String description, boolean active, List<Item> items, List<Menu> subMenus) {
        this.description = description;
        this.active = active;
        this.items = items;
        this.subMenus = subMenus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Menu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(List<Menu> subMenus) {
        this.subMenus = subMenus;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "description='" + description + '\'' +
                ", active=" + active +
                ", items=" + items +
                ", subMenus=" + subMenus +
                '}';
    }
}
