package com.example.demo.domains;

import java.util.List;

public class ServiceAvailability {
    private List<Days> days;
    private boolean enabled;
    private TimePeriod timePeriod;

    public ServiceAvailability(List<Days> days, boolean enabled, TimePeriod timePeriod) {
        this.days = days;
        this.enabled = enabled;
        this.timePeriod = timePeriod;
    }

    public List<Days> getDays() {
        return days;
    }

    public void setDays(List<Days> days) {
        this.days = days;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public TimePeriod getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(TimePeriod timePeriod) {
        this.timePeriod = timePeriod;
    }

    @Override
    public String toString() {
        return "ServiceAvailability{" +
                "days=" + days +
                ", enabled=" + enabled +
                ", timePeriod=" + timePeriod +
                '}';
    }
}
