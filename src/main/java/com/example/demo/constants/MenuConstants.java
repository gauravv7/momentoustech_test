package com.example.demo.constants;

public class MenuConstants {

    public static final String GROUP_BY_PRICE = "price";
    public static final String GROUP_BY_RANK = "rank";
}
